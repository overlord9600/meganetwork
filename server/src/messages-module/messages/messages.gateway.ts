import { Messages } from "src/entities/messages.entity";
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from "@nestjs/common";
import { MessageBody, OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, SubscribeMessage, WebSocketGateway, WebSocketServer } from "@nestjs/websockets";
 import { Socket, Server } from "socket.io";
import { User } from 'src/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
@WebSocketGateway()
export class MessageGateWay implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit{
    // @WebSocketServer() server:Server;
     constructor(
         @InjectRepository(User)
         private userRepository: Repository<User>,
         @InjectRepository(Messages)
         private messageRepository:Repository<Messages>
     ){}

    @SubscribeMessage('startChat')
    async startChat(client: Socket, data){
        let user = await this.userRepository.findOne({token:data.token})
        if(user){
            let messages = await this.messageRepository.find({
                where:[
                          {messageGetter:user.id,messageSender:data.friendId},
                          {messageGetter:data.friendId,messageSender:user.id}
                      ]
            })
            client.broadcast.emit('startChat', {messages,me:user.id})
            
        }
    }

    @SubscribeMessage('sendMessage')
    async sendMessage(client: Socket, data){
        let user = await this.userRepository.findOne({token:data.token})
        if(user){
             let message = await this.messageRepository.save({messageSender:user.id,messageGetter:data.friendId,message:data.text})             
                client.server.emit('message',{message,me:user.id})
        }
    }
    // async handleEvent(@MessageBody() body: any): Promise<any> {
    //         let user = await this.userRepository.findOne({token:body.token})
    //         let messages = await this.messageRepository.find({
    //             where:[
    //                 {messageGetter:user.id,messageSender:body.friendId},
    //                 {messageGetter:body.friendId,messageSender:user.id}
    //             ]
    //         })
    //         // console.log(body);
            
    //         this.server.emit( 'getMessages',{messages,me:user.id} )
    //   }

    // @SubscribeMessage('compose')
    // async messageToServer(@MessageBody() body: any): Promise<any> {
    //     let user = await this.userRepository.findOne({token:body.token})
    //     if(user){
    //         // this.server.emit( 'getMessages',{messages,me:user.id} )
    //        return this.messageRepository.save({messageSender:user.id,messageGetter:body.friendId,message:body.text}).then(r=>{
    //            return this.server.sockets.emit('compose',r)
    //         })
            
    //     }
       
    // }

    afterInit(server: Server){
        console.log('Init')
    }

    handleConnection(client: Socket){
         
    }

    async handleDisconnect(client: Socket){
        //  const user = await this.userRepository.findOne({id:client.id})
    }

}